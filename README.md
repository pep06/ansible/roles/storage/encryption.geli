# Ansible role ``storage/encryption.geli``

## Description

Configure storage partition encryption with FreeBSD's GELI framework.
